/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hal_token.h"
#include "ohos_errno.h"
#include "ohos_types.h"
#include <stdbool.h>
#include <stdint.h>
#include "utils_file.h"
//#include "bl_flash.h"
extern int bl_flash_erase(uint32_t addr, int len);
extern int bl_flash_write(uint32_t addr, uint8_t *src, int len);
extern int bl_flash_read(uint32_t addr, uint8_t *dst, int len);

//#define USE_MUTEX_FLASH_API 0

#define SAVE_TOKEN_MEM_ADDR_A 0x1FC000   // TOKEN A 区地址
#define SAVE_TOKEN_MEM_ADDR_B 0x1FD000   // TOKEN B 区地址
#define SAVE_TOKEN_MEM_ADDR_FLAG 0x1FB000  // TOEKN flag区地址
#define SAVE_TOKEN_MEM_ADDR 0x1FA000 // 预制token区域

#define WRITE_FLASH_MAX_TEMPERATURE 80  // ????3??°D′flash?á?μμí flash2áD′′?êyoíêy?Y±￡′?ê±??

#define HAL_TOKEN_SAVE_HAED_LEN 9
#define HAL_TOKEN_SAVE_HAED_MAGIC_LEN 8
#define HAL_TOKEN_SAVE_HAED_FLAG_LEN 1
#define HAL_TOKEN_SAVE_HAED_MAX_LEN 64

#define HAL_KITFWK_PRESET_TOKEN_FLAG 0xAA
#define HAL_PRESET_TOKEN_FLAG 0xEF
#define HAL_SAVE_TOKEN_A_FLAG 1
#define HAL_SAVE_TOKEN_B_FLAG 2

#define HAL_NOT_PRESET_TOKEN_TYPE -2
#define HAL_PRESET_TOKEN_TYPE 1
#define HAL_UPDATE_TOKEN_TYPE 0

#define HAL_TOKEN_MODULE_FAIL -1
#define HAL_TOKEN_MODULE_SUCCESS 0
#define ONE_PAGE_SIZE 0x1000
const unsigned char g_tokenSaveHeadMagic[HAL_TOKEN_SAVE_HAED_MAGIC_LEN] = {'h', 'i', 'f', 'l', 'y', 'i','n','g'};

 /*static bool HalIsHighTemperature()
{
   hi_s16 temperature = 0;
    int ret = hi_tsensor_read_temperature(&temperature);
    if (ret != 0) {
        return true;
    }
    return ((temperature >= WRITE_FLASH_MAX_TEMPERATURE) ? true : false);
}*/

static bool IsOEMMagicInvalid(unsigned char flag[], unsigned int flagLen)
{
    if ((flag == NULL) || (flagLen < sizeof(g_tokenSaveHeadMagic))) {
        return true;
    }
 
    for (int i = 0; i < sizeof(g_tokenSaveHeadMagic); i++) {
        if (flag[i] != g_tokenSaveHeadMagic[i]) {
            return true;
        }
    }

    return false;
}

static int OEMGetTokenFlag(unsigned char flag[], unsigned int flagLen)
{
    if ((flag == NULL) || (flagLen != HAL_TOKEN_SAVE_HAED_LEN)) {
        return HAL_TOKEN_MODULE_FAIL;
    }
    unsigned char localFlag[flagLen];
    int localFlagLen = sizeof(localFlag);
    (void)memset_s(localFlag, localFlagLen, 0, localFlagLen);
    int ret = bl_flash_read(SAVE_TOKEN_MEM_ADDR_FLAG,localFlag,localFlagLen);
    if(ret !=0 ) {
        printf("flash read fail, ret = %d\n", ret);
    }
    ret = memcpy_s(flag, flagLen, localFlag, localFlagLen);
    if (ret != 0) {
        printf("OEMGetTokenFlag memcpy_s fail, ret=%d\n", ret);
    }
    return ret;
}

static int OEMGetTokenType(unsigned char flag[], unsigned int flagLen)
{
    if ((flag == NULL) || (flagLen != HAL_TOKEN_SAVE_HAED_LEN)) {
        return HAL_TOKEN_MODULE_FAIL;
    }

    if (IsOEMMagicInvalid(flag, flagLen)) {
        return HAL_NOT_PRESET_TOKEN_TYPE;
    }

    unsigned char ch = flag[HAL_TOKEN_SAVE_HAED_LEN - 1];
    switch (ch) {
        case HAL_KITFWK_PRESET_TOKEN_FLAG:
            return HAL_PRESET_TOKEN_TYPE;
        case HAL_PRESET_TOKEN_FLAG:
            return HAL_PRESET_TOKEN_TYPE;
        case HAL_SAVE_TOKEN_A_FLAG:
            return HAL_UPDATE_TOKEN_TYPE;
        case HAL_SAVE_TOKEN_B_FLAG:
            return HAL_UPDATE_TOKEN_TYPE;
        default:
            return HAL_NOT_PRESET_TOKEN_TYPE;
    }
}

int OEMReadToken(char *token, unsigned int len)
{
    if ((token == NULL) || (len == 0))
	{
        return HAL_TOKEN_MODULE_FAIL;
    }

    unsigned char flag[HAL_TOKEN_SAVE_HAED_LEN] = {0};
    int ret = OEMGetTokenFlag(flag, sizeof(flag));
    if (ret != HAL_TOKEN_MODULE_SUCCESS) {
        return HAL_TOKEN_MODULE_FAIL;
    }

    int tokenType = OEMGetTokenType(flag, sizeof(flag));
    if (tokenType < 0) {
        return tokenType;
    }

    unsigned int readTokenAddr;
    if ((tokenType == HAL_PRESET_TOKEN_TYPE) && (flag[HAL_TOKEN_SAVE_HAED_LEN - 1] == HAL_KITFWK_PRESET_TOKEN_FLAG)) {
        readTokenAddr = SAVE_TOKEN_MEM_ADDR_A;
    } else if ((tokenType == HAL_PRESET_TOKEN_TYPE) && (flag[HAL_TOKEN_SAVE_HAED_LEN - 1] == HAL_PRESET_TOKEN_FLAG)) {
        readTokenAddr = SAVE_TOKEN_MEM_ADDR;
    } else if ((tokenType == HAL_UPDATE_TOKEN_TYPE) && (flag[HAL_TOKEN_SAVE_HAED_LEN - 1] == HAL_SAVE_TOKEN_A_FLAG)) {
        readTokenAddr = SAVE_TOKEN_MEM_ADDR_A;
    } else if ((tokenType == HAL_UPDATE_TOKEN_TYPE) && (flag[HAL_TOKEN_SAVE_HAED_LEN - 1] == HAL_SAVE_TOKEN_B_FLAG)) {
        readTokenAddr = SAVE_TOKEN_MEM_ADDR_B;
    } else {
        return HAL_TOKEN_MODULE_FAIL;
    }
    int readRet = bl_flash_read(readTokenAddr, token,len);
    if(readRet != 0) {
        printf("flash read fail%d\r\n", readRet);
    }
	token[len]=0;
	//printf("OEMReadToken tokenType=%d\r\n",tokenType);
	//printf("OEMReadToken len=%d token=%s\r\n",len,token);
	
	return tokenType;
}

int OEMWriteToken(const char *token, unsigned int len)
{
    if ((token == NULL) || (len == 0)) {
        return HAL_TOKEN_MODULE_FAIL;
    }

    unsigned char flag[HAL_TOKEN_SAVE_HAED_LEN] = {0};
    int ret = OEMGetTokenFlag(flag, sizeof(flag));
    if (ret != HAL_TOKEN_MODULE_SUCCESS)
	{
		printf("OEMGetTokenFlag fail\n");
        return HAL_TOKEN_MODULE_FAIL;
    }

    int tokenType = OEMGetTokenType(flag, sizeof(flag));
    if ((tokenType < 0) && (tokenType != HAL_NOT_PRESET_TOKEN_TYPE))
	{
		printf("OEMGetTokenType fail\nOEMGetTokenType=%02x\n",tokenType);
        return tokenType;
    }

    ret = memcpy_s(flag, sizeof(flag), g_tokenSaveHeadMagic, sizeof(g_tokenSaveHeadMagic));
    if (ret != 0) {
        printf("Memcpy_s magic number fail, ret=%d\n", ret);
        return HAL_TOKEN_MODULE_FAIL;
    }

    unsigned int writeTokenAddr;
    if (tokenType == HAL_PRESET_TOKEN_TYPE) {
        writeTokenAddr = SAVE_TOKEN_MEM_ADDR_B;
        flag[HAL_TOKEN_SAVE_HAED_LEN - 1] = HAL_SAVE_TOKEN_B_FLAG;
    } else if (tokenType == HAL_NOT_PRESET_TOKEN_TYPE) {
        writeTokenAddr = SAVE_TOKEN_MEM_ADDR_B;
        flag[HAL_TOKEN_SAVE_HAED_LEN - 1] = HAL_SAVE_TOKEN_B_FLAG;
    } else if ((tokenType == HAL_UPDATE_TOKEN_TYPE) && (flag[HAL_TOKEN_SAVE_HAED_LEN - 1] == HAL_SAVE_TOKEN_A_FLAG)) {
        writeTokenAddr = SAVE_TOKEN_MEM_ADDR_B;
        flag[HAL_TOKEN_SAVE_HAED_LEN - 1] = HAL_SAVE_TOKEN_B_FLAG;
    } else if ((tokenType == HAL_UPDATE_TOKEN_TYPE) && (flag[HAL_TOKEN_SAVE_HAED_LEN - 1] == HAL_SAVE_TOKEN_B_FLAG)) {
        writeTokenAddr = SAVE_TOKEN_MEM_ADDR_A;
        flag[HAL_TOKEN_SAVE_HAED_LEN - 1] = HAL_SAVE_TOKEN_A_FLAG;
    } else {
        return HAL_TOKEN_MODULE_FAIL;
    }


   /* if (HalIsHighTemperature()) {
		return HAL_TOKEN_MODULE_FAIL;
    }*/
	bl_flash_erase(writeTokenAddr,ONE_PAGE_SIZE);
    int writeRet = bl_flash_write(writeTokenAddr, token, len);
    if(writeRet != 0) {
        printf("flash read fail, writeRet = %d\r\n", writeRet);
    }
    bl_flash_erase(SAVE_TOKEN_MEM_ADDR_FLAG,ONE_PAGE_SIZE);
    writeRet = bl_flash_write(SAVE_TOKEN_MEM_ADDR_FLAG, flag, sizeof(flag));
    if(writeRet != 0) {
	printf("flash read fail, writeRet = %d\r\n", writeRet);
    }
    
    return HAL_TOKEN_MODULE_SUCCESS;
}

static int OEMGetAcKey(char *acKey, unsigned int len)
{
    // OEM need add here, get AcKey
    if ((acKey == NULL) || (len == 0)) {
        return EC_FAILURE;
    }
	
    const char acKeyBuf[] = {
		0x73,0x4A,0x68,0x4E,0x3B,0x77,0x2A,0x42,0x33,0x74,0x6D,0x50,0x3C,0x6F,0x2E,0x60,
        0x40,0xCA,0x74,0x27,0x25,0xB7,0x1A,0x72,0x41,0xCD,0xAB,0xD4,0x04,0x55,0x7D,0xC4,
        0x2B,0x32,0x7E,0xB2,0x39,0xB1,0xDC,0x28,0x46,0xBB,0x6A,0x77,0x86,0x3D,0xD8,0xE3};
       /* 0x3a,0x58,0x2f,0x2b,0x58,0x46,0x69,0x6c,0x64,0x5b,0x6a,0x7a,0x6e,0x6e,0x39,0x49,
        0x3f,0xde,0x1f,0xa9,0x11,0xe5,0x42,0x0a,0x4a,0x94,0x76,0x6b,0xc5,0x10,0xbf,0xd8,
        0x2f,0x34,0x84,0x67,0xf8,0xdd,0xdf,0x7d,0xec,0x6e,0xaa,0xd7,0x6f,0x1a,0xf2,0xec};*/
    int acKeyBufLen = sizeof(acKeyBuf);
    if (len < acKeyBufLen) {
        return EC_FAILURE;
    }

    int ret = memcpy_s(acKey, len, acKeyBuf, acKeyBufLen);
    if (ret != 0) {
        printf("OEMGetAcKey memcpy_s faile, ret=%d\n", ret);
    }
    return ret;
}
//#include "jydevinfo.h"
static int OEMGetProdId(char *productId, unsigned int len)
{
    // OEM need add here, get ProdId
    if ((productId == NULL) || (len == 0)) {
        return EC_FAILURE;
    }
	
    const char productIdBuf[] =   "23F9"; //"990Z";
    int productIdLen = sizeof(productIdBuf) - 1;

    if (len < productIdLen)
	{
        return EC_FAILURE;
    }
    int ret = memcpy_s(productId, len, productIdBuf, productIdLen);
    if (ret != 0) {
        printf("OEMGetProdId memcpy_s faile, ret=%d\n", ret);
    }
    return ret;
}

static int OEMGetProdKey(char *productKey, unsigned int len)
{
    // OEM need add here, get ProdKey
    if ((productKey == NULL) || (len == 0)) {
        return EC_FAILURE;
    }

    const char productKeyBuf[] = "517c13c25f01465fb02ada4e90512bf3";
    int productKeyLen = sizeof(productKeyBuf) - 1;
    if (len < productKeyLen) 
	{
        return EC_FAILURE;
    }

    int ret = memcpy_s(productKey, len, productKeyBuf, productKeyLen);
    if (ret != 0) {
        printf("OEMGetProdKey memcpy_s fail, ret=%d\n", ret);
    }
    return ret;
}

int HalReadToken(char *token, unsigned int len)
{
    if (token == NULL) {
        return EC_FAILURE;
    }

    return OEMReadToken(token, len);
}

int HalWriteToken(const char *token, unsigned int len)
{
    if (token == NULL) 
	{
        return EC_FAILURE;
    }

    return OEMWriteToken(token, len);
}
int HalReadTokenFlag(char *flag, unsigned int len)
{
	 bl_flash_read(SAVE_TOKEN_MEM_ADDR_FLAG, flag, len);
}

int HalWriteTokenFlag(char *flag, unsigned int len)
{
	if(flag == NULL) 
	{
        return EC_FAILURE;
    }
    bl_flash_erase(SAVE_TOKEN_MEM_ADDR_FLAG,ONE_PAGE_SIZE);
    bl_flash_write(SAVE_TOKEN_MEM_ADDR_FLAG, flag, len);
    return 0;
}
int HalGetAcKey(char *acKey, unsigned int len)
{
    if (acKey == NULL) {
        return EC_FAILURE;
    }

    return OEMGetAcKey(acKey, len);
}

int HalGetProdId(char *productId, unsigned int len)
{
    if (productId == NULL)
	{
        return EC_FAILURE;
    }

    return OEMGetProdId(productId, len);
}

int HalGetProdKey(char *productKey, unsigned int len)
{
    if (productKey == NULL) 
	{
        return EC_FAILURE;
    }

    return OEMGetProdKey(productKey, len);
}
